#include <jni.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

using namespace cv;
using namespace std;

extern "C" {

JNIEXPORT void JNICALL Java_com_xubiker_phobostesttask_MainActivity_proceed(JNIEnv*, jobject, jlong addrGray, jlong addrRgba)
{
    Mat& mGr  = * (Mat*) addrGray;
    Mat& mRgb = *(Mat*)addrRgba;

    vector<KeyPoint> v;

    Ptr<xfeatures2d::SURF> detector = xfeatures2d::SURF::create();
//    Ptr<FeatureDetector> detector = FastFeatureDetector::create();
    detector->detect(mGr, v);
    for( unsigned int i = 0; i < v.size(); i++ )
    {
        const KeyPoint& kp = v[i];
        circle(mRgb, Point((int) kp.pt.x, (int) kp.pt.y), 10, Scalar(255, 0, 0, 255));
    }
}

JNIEXPORT jstring JNICALL
Java_com_xubiker_phobostesttask_MainActivity_getMsgFromJni(JNIEnv *env, jobject instance) {

    return env->NewStringUTF("Hello from jni");
}

}